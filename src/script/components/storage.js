class Storage {
  constructor(data = {}) {
    this._storage = data.storage || sessionStorage;
    this.setResource(data.resource || this._storage.getItem("resource"));
  }

  getResource() {
    return this._resource;
  }

  getItem() {
    return this._storage.getItem(this._resource);
  }

  setResource(resource) {
    resource && (this._storage.setItem("resource", resource), this._resource = resource);
    return this; 
  }

  setItem(item) {
    item && this._storage.setItem(this._resource, item);
    return this;
  }

  removeItem(item) {
    item && this._storage.removeItem(item);
  }

  clear() {
    this._storage.clear();
  }  
}