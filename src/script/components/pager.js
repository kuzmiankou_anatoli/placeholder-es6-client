class Pager {
  constructor(data = {}) {
    this.setPageDataHandler(data.handler);
  
    this._pages = document.createElement('ul');
    this._pages.classList.add("pager-items");
    this._visblePageCount = (data.visblePageCount >= this.MIN_VISIBLE_PAGE_COUNT) 
      ? data.visblePageCount : this.MIN_VISIBLE_PAGE_COUNT;
    this._elemsCountOnPage = data.elemsCountOnPage || this.DEFAULT_ELEMS_COUNT_ON_PAGE;
    
    this.setElems(data.elems);
    this.setPagerId(data.pagerId);

    this._pageClick = this._pageClick.bind(this);
  }

  get MIN_VISIBLE_PAGE_COUNT() { return 7; }
  get DEFAULT_ELEMS_COUNT_ON_PAGE() { return 12; }
  get DEFAULT_ACTIVE_PAGE() { return 1; }
  get PAGES_STEP() { return 4; }
  get DEFAULT_PAGER_ID() { return 'pager'; }

  setElems(elems = []) {
    this._elems = elems;
    this._setPagesCount(Math.ceil(elems.length / this._elemsCountOnPage));
    this.setActivePage(this._activePage);
    return this;
  }

  _defaultPageHandler(data) {
    console.log(data);
  }

  setPageDataHandler(handler) {
    this._pageHandler = handler || this._defaultPageHandler;
    return this;
  }

  setPagerId(pagerId) {
    const container = document.getElementById(pagerId || this.DEFAULT_PAGER_ID);
    container && container.appendChild(this._pages);
    return this;
  }

  setActivePage(number) {
    this._activePage = number;
    this._activePage < this.DEFAULT_ACTIVE_PAGE && (this._activePage = this.DEFAULT_ACTIVE_PAGE);
    this._activePage > this._pagesCount && (this._activePage = this.pagesCount);
    this._createPages();
    this._pageHandler(this.getElems(this._activePage));
    return this;
  }

  getActivePage() {
    return this._activePage;
  }

  _buildPage(number, name) {
    const elem = document.createElement('li');
    elem.classList.add('pager-item');
    if(number > 0) {
      elem.dataset.pagerTarget = number;
      elem.innerHTML = `<a href='page-${ number })'>${ name || number || '...' }</a>`;
      elem.addEventListener('click', this._pageClick);
    }
    return elem;
  }

  _buildLittle() {
    for(var i = 0; i < this._pagesCount;){
      this._pages.appendChild(this._buildPage(++i));
    }
    this._activePage && this._pages.childNodes[this._activePage -1].classList.add('active');
  }

  _buildLeft() {
    for(var i = 0; i < this._visblePageCount - 2;){
      this._pages.appendChild(this._buildPage(++i));
    }
    this._pages.appendChild(this._buildPage());
    this._pages.appendChild(this._buildPage(this._pagesCount));
    this._pages.childNodes[this._activePage - 1].classList.add('active');
  }

  _buildRight() {
    this._pages.appendChild(this._buildPage(1));
    this._pages.appendChild(this._buildPage());
    var hidePages = this._pagesCount - this._visblePageCount + 1;
    for(var i = hidePages + 1; i < this._pagesCount;){
      this._pages.appendChild(this._buildPage(++i));
    }
    this._pages.childNodes[this._activePage - hidePages].classList.add('active');
  }

  _buildCenter() {
    this._pages.appendChild(this._buildPage(1));
    this._pages.appendChild(this._buildPage());
    const halfCount = parseInt(this._visblePageCount / 2);
    let i = parseInt(this._activePage - halfCount) + 1;
    const count = i + this._visblePageCount - this.PAGES_STEP;
    while(i < count){
      this._pages.appendChild(this._buildPage(++i));
    }
    this._pages.appendChild(this._buildPage());
    this._pages.appendChild(this._buildPage(this._pagesCount));
    this._pages.childNodes[halfCount].classList.add('active');
  }

  _createPages() {
    this._pages.innerHTML = "";
    const halfCount = this._visblePageCount / 2;
    if(this._pagesCount <= this._visblePageCount) {
      this._buildLittle();
    }
    else if(this._activePage < halfCount) {
      this._buildLeft();
    }
    else if(this._activePage > this._pagesCount - halfCount + 1) {
      this._buildRight();
    }
    else {
      this._buildCenter();
    }
  }

  _setPagesCount(count) {
    this._pagesCount = count;
 }

  getElems(pageNumber) {
    let first, last;
    if(pageNumber) {
      last = pageNumber * this._elemsCountOnPage;
      first = last - this._elemsCountOnPage;
    }
    else {
      first = 0;
      last = this._elems.length;
    }
    return this._elems.slice(first, last);
  }

  _pageClick(e) {
    e.preventDefault();
    this.setActivePage(e.currentTarget.dataset.pagerTarget);
  }
}