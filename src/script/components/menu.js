class Menu {
  constructor(id = "menu", data = {}) {
    this._menu = document.getElementById(id);
    
    this._defaultItemHandler = data.itemHandler || function(e){};
  
    for(let item of this._menu.querySelectorAll(".menu-item")) {
      item.addEventListener('click', data[item.dataset.target + "ItemHandler"] || this._defaultItemHandler);
    }
  }

  addItem(name, target, handler) {
    const useHandler = handler || this._defaultItemHandler;
    const elem = document.createElement('li');
    elem.className = `menu-item menu-item-${ target }`;
    elem.dataset.target = target;
    elem.innerHTML = `<a href='#${ target }'>${ name }</a>`;
    elem.addEventListener('click', useHandler);
    this._menu.appendChild(elem);
  }
}