class Request {
  constructor(data = {}) {
    this._method = data.method || "get";
    this._url = data.url || window.location.pathname;
    
    this._defaultHandler = data.handler || this._defaultHandler;
    this._defaultErrorHandler = data.errorHandler || this._defaultErrorHandler;

    this._verifyResponse = this._verifyResponse.bind(this);
  }

  _defaultHandler(data){
    console.log(data);
  };

  _defaultErrorHandler(data) {
    console.log(data);
  }

  get FIRST_VALID_STATUS() { return 200 };
  get END_VALID_STATUS() { return 300 };

  _serialParams(params) {
    let result = "";
    for(let i in params) {
      result += `${ i }=${ encodeURIComponent(params[i])}&`;
    }
    return result.substring(0, result.length - 1);
  }

  _verifyResponse(response) {
    if(response.status >= this.FIRST_VALID_STATUS && response.status < this.END_VALID_STATUS) {
      return response;
    } 
    else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  _getResponseData(response) {
    return response.text();
  }

  send(method, url, data, handler, errorHandler) {
    let setting = { method: method || this._method }
    let target = url || this._url;
    let serialData = this._serialParams(data);

    if(setting.method == 'get') {
      target += ((!target || (target.indexOf('?') < 0)) ? "?" : "&") + serialData;
    }
    else {
      setting.headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
      setting.body = serialData;
    }

    fetch(target, setting)
      .then(this._verifyResponse)
      .then(this._getResponseData)
      .then(handler || this._defaultHandler)
      .catch(errorHandler || this._defaultErrorHandler);
    
    return this;
  }

  sendPost(url, data, handler, errorHandler) {
    return this.send("post", url, data, handler, errorHandler);
  }
  
  sendGet(url, data, handler, errorHandler) {
    return this.send("get", url, data, handler, errorHandler);
  }
  
  sendPut(url, data, handler, errorHandler) {
    return this.send("put", url, data, handler, errorHandler);
  }
  
  sendDelete(url, data, handler, errorHandler) {
    return this.send("delete", url, data, handler, errorHandler);
  }
}