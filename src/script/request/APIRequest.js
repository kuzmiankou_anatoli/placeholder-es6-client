class APIRequest extends Request {
  constructor(data = {}) {
    super(data);

    this._url = data.url || "https://api.nytimes.com/svc/topstories/v2/";
    this._defaultApiKey = data.apiKey;
    this._defaultGetHandler = data.getHandler;
    this._defaultTarget = data.defaultTarget || "home"
  }

  getResource(target, apiKey, handler, errorHandler) {
    let useHandler = handler || this._defaultGetHandler;
    let useApiKey = apiKey || this._defaultApiKey;
    let useTarget = target || this._defaultTarget;
    return this.sendGet(`${ this._url + useTarget }.json`, {
      "api-key": useApiKey
    }, useHandler, errorHandler);
  }
}