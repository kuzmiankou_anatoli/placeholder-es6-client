class JSONPlaceholderRequest extends Request {
  constructor(data = {}) {
    super(data);

    this._url = data.url || "http://jsonplaceholder.typicode.com";
    this._defaultGetHandler = data.getHandler;
    this._defaultCreateHandler = data.createHandler;
    this._defaultUpdateHandler = data.updateHandler;
    this._defaultRemoveHandler = data.removeHandler;
    this._defaultGetPostsHandler = data.getPostsHandler;
    this._defaultGetAlbumsHandler = data.getAlbumsHandler;
    this._defaultGetPostCommentsHandler = data.getPostCommentsHandler;
    this._defaultGetAlbumPhotosHandler = data.getAlbumPhotosHandler;
    this._defaultGetPostHandler = data.getPostHandler;
    this._defaultGetUsersHandler = data.getUsersHandler;
    this._defaultUpdatePostHandler = data.updatePostHandler;
    this._defaultCreatePostHandler = data.createPostHandler;
  }

  _serialPath(name, number, target) {
    let url = "";
    name && (url += `/${ name }`) && 
    number && (url += `/${ number }`) && 
    target && (url += `/${ target }`);
    return url;
  }

  send(method, url, data, handler, errorHandler) {
    let path = this._url + (url || "/posts");
    return super.send(method, path, data, handler, errorHandler);
  }

  getResource(name, number, target, handler, errorHandler) {
    let useHandler = handler || this._defaultGetHandler;
    let path = this._serialPath(name, number, target);
    return this.sendGet(path, null, useHandler, errorHandler);
  }

  getPosts(handler, errorHandler) {
    let useHandler = handler || this._defaultGetPostsHandler;
    return this.getResource("posts", null, null, useHandler, errorHandler);
  }

  getAlbums(handler, errorHandler) {
    let useHandler = handler || this._defaultGetAlbumsHandler;
    return this.getResource("albums", null, null, useHandler, errorHandler);
  }

  getPost(number, handler, errorHandler) {
    let useHandler = handler || this._defaultGetPostHandler;
    return this.getResource("posts", number, null, useHandler, errorHandler);
  }

  getPostComments(number, handler, errorHandler) {
    let useHandler = handler || this._defaultGetPostCommentsHandler;
    return this.getResource("posts", number, "comments", useHandler, errorHandler);
  }

  getAlbumPhotos(number, handler, errorHandler) {
    let useHandler = handler || this._defaultGetAlbumPhotosHandler;
    return this.getResource("albums", number, "photos", useHandler, errorHandler);
  }

  getUsers(handler, errorHandler) {
    let useHandler = handler || this._defaultGetUsersHandler;
    return this.getResource("users", null, null, useHandler, errorHandler);
  }

  createResource(name, data, handler, errorHandler) {
    let useHandler = handler || this._defaultCreateHandler;
    let path = this._serialPath(name);
    return this.sendPost(path, data, useHandler, errorHandler);
  }

  createPost(name, data, handler, errorHandler) {
    let useHandler = handler || this._defaultCreatePostHandler;
    return this.createResource("posts", data, useHandler, errorHandler);
  }

  updateResource(name, number, data, handler, errorHandler) {
    let useHandler = handler || this._defaultUpdateHandler;
    let path = this._serialPath(name, number)
    return this.sendPut(path, data, useHandler, errorHandler);
  }

  updatePost(number, data, handler, errorHandler) {
    let useHandler = handler || this._defaultUpdatePostHandler;
    return this.updateResource("posts", number, data, useHandler, errorHandler);
  }

  removeResource(name, number, handler, errorHandler) {
    var useHandler = handler || this._defaultRemoveHandler;
    var path = this._serialPath(name, number);
    return this.send("delete", path, null, useHandler, errorHandler);
  }
}