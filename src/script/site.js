;(function(data){

  function hideContainers(){
    const elem = document.querySelector('[data-hidden="false"]');
    elem && (elem.dataset.hidden = true);
  }

  const Builder = data.Builder;

  const storage = new data.Storage();

  const pager = new data.Pager();

  const handlers = {
    postPageData: function(data) {
      storage.setItem(pager.getActivePage());
      const elem = document.getElementById('content');
      elem.innerHTML = "";
      hideContainers();
      elem.parentNode.dataset.hidden = false;
      Builder.buildPosts(data).forEach(function(item, index){
        elem.appendChild(item);
        item.addEventListener('click', handlers.postClick);
      })
    },

    postClick: function(e) {
      e.preventDefault();
      hideContainers();
      document.getElementById("post").dataset.hidden = false;
      request.getPost(e.currentTarget.dataset.id);
      request.getPostComments(e.currentTarget.dataset.id);
    },

    albumsPageData: function(data) {
      const elem = document.getElementById('content');
      elem.innerHTML = "";
      hideContainers();
      elem.parentNode.dataset.hidden = false;
      Builder.buildAlbums(data).forEach(function(item){
        elem.appendChild(item);
        item.addEventListener('click', handlers.albumClick);
      })
    },

    albumClick: function(e) {
      request.getAlbumPhotos(e.currentTarget.dataset.id);
    }
  }

  const request = new data.Request({
    getPostsHandler: function(data){
      pager.setPageDataHandler(handlers.postPageData);
      pager.setElems(JSON.parse(data));
      pager.setActivePage(storage.getItem());
    },

    getAlbumsHandler: function(data){
      storage.setItem(pager.getActivePage());
      pager.setPageDataHandler(handlers.albumsPageData);
      pager.setElems(JSON.parse(data));
      pager.setActivePage(storage.getItem());
    },

    getPostCommentsHandler: function(data){
      const elem = document.getElementById('comments');
      elem.innerHTML = "";
      hideContainers();
      elem.parentNode.dataset.hidden = false;
      Builder.buildComments(JSON.parse(data)).forEach(function(item){
        elem.appendChild(item);
      })
    },

    getAlbumPhotosHandler: function(data){
      const elem = document.getElementById('photos');
      hideContainers();
      elem.dataset.hidden = false;
      Builder.buildPhotos(JSON.parse(data)).forEach(function(item){
        elem.appendChild(item);
      })
    },

    getPostHandler: function(data){
      const dataJSON = JSON.parse(data);
      const form = document.getElementById("post-form");
      form.dataset.target = "update";
      for(let i in dataJSON){
        form.elements[i].value = dataJSON[i];
      }
    },

    updatePostHandler: function(data){
      alert(data);
    },

    createPostHandler: function(data){
      alert(data);
    },

    getUsersHandler: function(data){
      const userElement = document.getElementById('users');
      JSON.parse(data).forEach(function(item){
        userElement.appendChild(Builder.buildOption(item.name, item.id));
      });
    }
  });

  const apiRequest = new APIRequest({
    apiKey: "eb61e7f9644042a3ba9eb7b1717b5289",
    getHandler: function(data){
      const elem = document.getElementById('articles-content');
      elem.innerHTML = "";
      hideContainers();
      elem.parentNode.dataset.hidden = false;
      Builder.buildArticles(JSON.parse(data).results).forEach(function(item){
        elem.appendChild(item);
      });
    }
  });

  const mainMenu = new Menu("main-menu", {
    postsItemHandler: function(e){
      e.preventDefault();
      storage.setResource(e.currentTarget.dataset.target);
      const target = storage.getItem();
      pager.setActivePage(target);
      request.getPosts();
    },
    albumsItemHandler: function(e){
      e.preventDefault();
      storage.setResource(e.currentTarget.dataset.target);
      const target = storage.getItem();
      pager.setActivePage(target);
      request.getAlbums();
    },
    articlesItemHandler: function(e){
      e.preventDefault();
      storage.setResource(e.currentTarget.dataset.target);
      const target = storage.getItem();
      apiRequest.getResource(target);
    },
    addPostItemHandler: function(e){
      e.preventDefault();
      hideContainers();
      document.getElementById("post").dataset.hidden = false;
      const elem = document.getElementById('comments');
      elem.innerHTML = "";
      const form = document.getElementById("post-form");
      form.dataset.target = "create";
      form.elements.id.value = "";
      form.elements.userId.value = "";
      form.elements.title.value = "";
      form.elements.body.value = "";
    }
  })

  const menu = new Menu("articles-menu", {
    itemHandler: function(e){
      e.preventDefault();
      const target = e.currentTarget.dataset.target;
      storage.setItem(target);
      apiRequest.getResource(target);
    }
  });

  document.getElementById('post-form').addEventListener('submit', function(e){
    e.preventDefault();
    const data = {};
    const elems = e.currentTarget.elements;
    data.userId = elems.userId.value;
    data.title = elems.title.value;
    data.body = elems.body.value;
    request[`${ e.currentTarget.dataset.target }Post`](elems.id.value, data);
  });

  request.getUsers();

  const target = location.search;

  switch(target.substr(1, target.length) || storage.getResource()){
    case "albums":
      request.getAlbums();
      break;
    case "articles":
      apiRequest.getResource();
      break;
    default:
      request.getPosts();
  }

})({
  Request: JSONPlaceholderRequest,
  APIRequest: APIRequest,
  Pager: Pager,
  Menu: Menu,
  Storage: Storage,

  Builder: Builder
});