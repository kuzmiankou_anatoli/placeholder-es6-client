class Builder {

  static createElement(type, text = "", attributes, children){
    let elem = document.createElement(type);
    elem.innerHTML = text;
    if(children) { 
      for(var i in children) {
        elem.appendChild(children[i]);
      }
    }
    if(attributes) {
      for(var i in attributes) {
        elem.setAttribute(i, attributes[i]);
      }
    }
    return elem;
  }

  static createBody(text) {
    return Builder.createElement('p', text, {class: "body"});
  }

  static createTitle(text) {
    return Builder.createElement('h2', text, {class: "title"});
  }

  static createEmail(text) {
    return Builder.createElement('p', `<a href="mailto:${ text }">${ text }</a>`, {class: "email"});
  }

  static createLink(url, text, className) {
    return Builder.createElement('a', text || url, {
      href: url,
      class: className
    });
  }

  static createImg(preview, url) {
    const setting = {
      "data-url": url,
      class: "img"
    }
    return Builder.createElement('p', `<img href="${ preview }">`, setting);
  }

  static buildOption(text, value) {
    return Builder.createElement('option', text, { value: value });
  }

  static buildArticle(data) {
    const setting = {
      class: "article-item"
    }
    const children = {
      title: Builder.createTitle(data.title),
      body: Builder.createBody(data.abstract)
    }
    return Builder.createElement('div', '', setting, children);
  }

  static buildArticles(articles) {
    return articles.map(item => Builder.buildArticle(item));
  }

  static buildPost (data) {
    const setting = {
      "data-user-id": data.userId,
      "data-id": data.id,
      class: "post"
    }
    const children = {
      title: Builder.createTitle(data.title),
      body: Builder.createBody(data.body)
    }
    return Builder.createElement('div', '', setting, children);
  }

  static buildPosts(posts) {
    return posts.map(item => Builder.buildPost(item));
  }

  static buildComment(data) {
    const setting = {
      "data-post-id": data.postId,
      "data-id": data.id,
      class: "comment"
    }
    const children = {
      title: Builder.createTitle(data.name),
      email: Builder.createEmail(data.email),
      body: Builder.createBody(data.body)
    }
    return Builder.createElement('div', '', setting, children);
  }

  static buildComments(comments) {
    return comments.map(item => Builder.buildComment(item));
  }

  static buildAlbum(data) {
    const setting = {
      "data-user-id": data.userId,
      "data-id": data.id,
      class: "album"
    }
    const children = {
      title: Builder.createTitle(data.title)
    }
    return Builder.createElement('div', '', setting, children);
  }

  static buildAlbums(albums) {
    return albums.map(item => Builder.buildAlbum(item));
  }

  static buildPhoto(data) {
    const setting = {
      "data-album-id": data.albumId,
      "data-id": data.id,
      class: "photo"
    }
    const children = {
      title: Builder.createTitle(data.title),
      img: Builder.createImg(data.thumbnailUrl, data.url)
    }
    return Builder.createElement('div', '', setting, children);
  }

  static buildPhotos(photos) {
    return photos.map(item => Builder.buildPhoto(item));
  }

  static buildElements(typeAction, data) {
    let result = undefined;
    if(typeAction != "buildElements") {
      Builder[typeAction] && (result = Builder[typeAction](data));
    }
    return result;
  }
}