const gulp = require('gulp');
const browserSync = require('browser-sync');

gulp.task('liveReload', function() {
  browserSync({
    server: {
      baseDir: __dirname + "/dest"
    },
    host: 'localhost',
    port: 3000,
    notify: false
  })
})

gulp.task('html', function() {
  return(gulp.src(__dirname + '/src/**/*.html')
    .pipe(gulp.dest(__dirname + '/dest')))
})

gulp.task('css', function() {
  return(gulp.src(__dirname + '/src/style/**/*.css')
    .pipe(gulp.dest(__dirname + '/dest/style'))
  );
})

gulp.task('js', function() {
  return(gulp.src(__dirname + '/src/script/**/*.js')
    .pipe(gulp.dest(__dirname + '/dest/script'))
  );
})

gulp.task('reload', function() {
  browserSync.reload();
})

gulp.task('watch', ['html', 'css', 'js', 'liveReload'], function() {
  gulp.watch(__dirname + '/src/index.html', ['html', 'reload']);
  gulp.watch(__dirname + '/src/script/**/*.js', ['js', 'reload']);
  gulp.watch(__dirname + '/src/style/**/*.css', ['css', 'reload']);
})